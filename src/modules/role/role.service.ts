import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { RoleRepository } from './role.repository';
import { Role } from './role.entity';

@Injectable()
export class RoleService {
  constructor(
    @InjectRepository(RoleRepository)
    private readonly roleRepository: RoleRepository,
  ) {}

  async get(id: number): Promise<Role> {
    if (!id) {
      throw new NotFoundException('id must be sent');
    }
    let role: Role;
    role = await this.roleRepository.findOne(id);
    if (!role) {
      throw new NotFoundException();
    }
    return role;
  }

  async getAll(): Promise<Role[]> {
    return  await this.roleRepository.find();
  }

  async create(role: Role): Promise<Role> {
    return  await this.roleRepository.save(role);
  }

  async update(id: number, role: Role): Promise<string> {
    if (!id || !role) {
      throw new NotFoundException('id and role must be sent');
    }
    const existRole = this.roleRepository.findOne(id);
    if (!existRole) {
      throw new NotFoundException('This role does not exist');
    }
    await this.roleRepository.update(id, role);
    return 'Role update succesfully';
  }
}
