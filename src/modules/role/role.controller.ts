import {
  Body,
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Patch,
  Post,
} from '@nestjs/common';
import { RoleService } from './role.service';
import { Role } from './role.entity';

@Controller('roles')
export class RoleController {
  constructor(private readonly roleService: RoleService) {}

  @Get(':id')
  async getRole(@Param('id', ParseIntPipe) id: number) {
    return  await this.roleService.get(id);
  }

  @Get()
  async getRoles() {
    return  await this.roleService.getAll();
  }

  @Post()
  async createRoles(@Body() role: Role) {
    return  await this.roleService.create(role);
  }
  @Patch(':id')
  async UpdateRole(@Body() role: Role, @Param('id', ParseIntPipe) id: number) {
    return  await this.roleService.update(id, role);

  }
}
