import { IsNotEmpty } from 'class-validator';
import { Role } from '../../role/role.entity';
import { Roletype } from '../../role/roletype.enum';
import { UserDetails } from '../user.details.entity';

export class UserDto {
  @IsNotEmpty()
  id: number;

  @IsNotEmpty()
  username: string;

  @IsNotEmpty()
  email: string;

  @IsNotEmpty()
  roles: Roletype[];

  @IsNotEmpty()
  details: UserDetails;
}
