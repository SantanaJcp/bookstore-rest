import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Patch,
  Post,
} from '@nestjs/common';
import { UserService } from './user.service';
import { User } from './user.entity';

@Controller('users')
export class UserController {
  constructor(private readonly userService: UserService) {}
  @Get(':id')
  async getUser(@Param('id', ParseIntPipe) id: number): Promise<User> {
    return  await this.userService.get(id);
  }

  @Get()
  async getUsers(): Promise<User[]> {
    return  await this.userService.getAll();
  }

  @Post()
  async createUser(@Body() user: User): Promise<User> {
    return await this.userService.create(user);
  }
  @Patch(':id')
  async updateUser(@Param('id', ParseIntPipe) id: number, @Body() user: User) {
    return  await this.userService.update(id, user);
  }
  @Delete(':id')
  async deleteUser(@Param('id', ParseIntPipe) id: number) {
    return  await this.userService.delete(id);
  }
}
