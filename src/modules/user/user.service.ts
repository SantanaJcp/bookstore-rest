import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { UserRepository } from './user.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './user.entity';
import { UserDetails } from './user.details.entity';
import { Role } from '../role/role.entity';
import { getRepository } from 'typeorm';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserRepository)
    private readonly userRepository: UserRepository,
  ) {}

  async get(id: number): Promise<User> {
    if (!id) {
      throw new BadRequestException('id must be sent');
    }

    const user: User = await this.userRepository.findOne(id, {
      where: { status: 'ACTIVE' },
    });

    if (!user) {
      throw new NotFoundException();
    }
    return user;
  }

  async getAll(): Promise<User[]> {
    return  await this.userRepository.find({
      where: { status: 'ACTIVE' },
    });
  }
  async create(user: User): Promise<User> {
    user.details = new UserDetails();
    const roles = getRepository(Role);
    user.roles =  await roles.find();
    return  await this.userRepository.save(user);
  }
  async update(id: number, user: User): Promise<void> {
    if (!id) {
      throw new BadRequestException('id must be sent');
    }
    const userExists = await this.userRepository.findOne(id, {
      where: { status: 'ACTIVE' },
    });
    if (!userExists) {
      throw new NotFoundException();
    }
    await this.userRepository.update(id, user);
  }
  async delete(id: number): Promise<void> {
    const userExists = await this.userRepository.findOne(id, {
      where: { status: 'ACTIVE' },
    });
    if (!userExists) {
      throw new NotFoundException();
    }
    await this.userRepository.update(id, { status: 'INACTIVE' });
  }
}
