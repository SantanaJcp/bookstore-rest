import {MigrationInterface, QueryRunner} from "typeorm";

export class thirdMigration1568905284920 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "status"`, undefined);
        await queryRunner.query(`ALTER TABLE "users" ADD "status" character varying(12) NOT NULL DEFAULT 'ACTIVE'`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "status"`, undefined);
        await queryRunner.query(`ALTER TABLE "users" ADD "status" character varying(8) NOT NULL DEFAULT 'ACTIVE'`, undefined);
    }

}
